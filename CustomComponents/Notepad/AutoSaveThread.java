package CustomComponents.Notepad;

public class AutoSaveThread extends Thread {
	MainFrame frame;

	public AutoSaveThread(MainFrame frame) {
		this.frame = frame;
		start();
	}

	public void run() {
		while(true) {
			if(frame.isModified && (! (! frame.previousStates.isEmpty() && frame.textarea.getText().equals(frame.previousStates.peek())) )) {
				frame.previousStates.push(frame.textarea.getText());
			}
			try {
				Thread.sleep(5000);
			} catch (Exception e) { System.out.println(e); }
		}
	}
}