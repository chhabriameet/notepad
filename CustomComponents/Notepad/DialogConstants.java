package CustomComponents.Notepad;

public interface DialogConstants {
	static final int CANCEL = 1;
	static final int YES = 2;
	static final int NO = 3;
}