package CustomComponents.Notepad;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FindReplaceDialog extends Dialog {
	MainFrame parent;
	TextField findText;
	TextField replaceText;
	Button replaceButton;

	FindReplaceDialog(MainFrame parent) {
		super(parent, "Find and Replace Dialog", false);
		this.parent = parent;
		findText = new TextField();
		replaceText = new TextField();
		replaceButton = new Button("Find and Replace");

		setLayout(new GridLayout(3, 1));
		Panel text1 = new Panel();
		text1.setLayout(new GridLayout(1, 2));
		text1.add(new Label("Find: "));
		text1.add(findText);

		Panel text2 = new Panel();
		text2.setLayout(new GridLayout(1, 2));
		text2.add(new Label("Replace: "));
		text2.add(replaceText);

		Panel buttons = new Panel();
		buttons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttons.add(replaceButton);

		add(text1);
		add(text2);
		add(buttons);

		registerListeners();

		setSize(300, 150);
		setVisible(true);
	}

	private void registerListeners() {
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				dispose();
			}
		});

		ActionEventHandler handler = new ActionEventHandler(parent, this);
		replaceButton.addActionListener(handler);
	}
}