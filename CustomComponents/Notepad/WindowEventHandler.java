package CustomComponents.Notepad;

import java.awt.FileDialog;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

public class WindowEventHandler extends WindowAdapter {
	MainFrame frame;
	public WindowEventHandler(MainFrame frame) {
		this.frame = frame;
	}
	public void windowClosing(WindowEvent we) {
		if(frame.isModified) {
			int result = ConfirmDialog.createDialog(frame, "Do you want to save the changes"); 
			if(result == DialogConstants.YES) {
				if(frame.fileOp == null) {
					FileDialog fd = new FileDialog(frame, "Save File", FileDialog.SAVE);
					fd.setVisible(true);
					while(fd.getDirectory() == null && fd.getFile() == null) {
						fd = new FileDialog(frame, "Save File", FileDialog.SAVE);
						fd.setVisible(true);
					}
					if(fd.getDirectory() != null && fd.getFile() != null) {
						String fileName = fd.getDirectory() + fd.getFile();
						if(! fileName.matches(".*\\.ht$"))
							fileName += ".ht";
						frame.fileOp = new FileOperations(fileName);
					}
				}
				frame.fileOp.fileWrite(frame.textarea.getText());
				System.exit(1);
			} else if(result == DialogConstants.NO)
				System.exit(1);
		} else
			System.exit(1);
	}
}