package CustomComponents.Notepad;

import java.awt.Frame;
import java.awt.TextArea;
import java.awt.MenuBar;
import java.awt.Menu;
import java.awt.MenuItem;

import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;

import java.util.Stack;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import java.util.ArrayList;

public class MainFrame extends Frame {
	TextArea textarea;
	MenuItem open;
	MenuItem save;
	MenuItem saveAs;
	MenuItem find;
	MenuItem exit;
	MenuItem replace;
	MenuItem undo;
	MenuItem redo;
	FileOperations fileOp;
	Matcher matcher;
	String previousSavedData;
	boolean isModified;
	Stack<String> previousStates;
	Stack<String> futureStates;
	
	public MainFrame() {
		super("Notepad");
		textarea = new TextArea("", 0, 0, TextArea.SCROLLBARS_VERTICAL_ONLY);
		previousSavedData = "";
		isModified = false;
		previousStates = new Stack<String>();
		futureStates = new Stack<String>();

		MenuBar mbar = new MenuBar();
		setMenuBar(mbar);

		Menu fileMenu = new Menu("File");
		Menu editMenu = new Menu("Edit");

		mbar.add(fileMenu);
		mbar.add(editMenu);

		open = new MenuItem("Open");
		save = new MenuItem("Save");
		save.setEnabled(false);
		saveAs = new MenuItem("Save As");
		exit = new MenuItem("Exit");

		fileMenu.add(open);
		fileMenu.addSeparator();
		fileMenu.add(save);
		fileMenu.add(saveAs);
		fileMenu.addSeparator();
		fileMenu.add(exit);

		undo = new MenuItem("Undo");
		redo = new MenuItem("Redo");
		redo.setEnabled(false);
		find = new MenuItem("Find");
		replace = new MenuItem("Replace");
		MenuItem selectAll = new MenuItem("Select All");

		editMenu.add(undo);
		editMenu.add(redo);
		editMenu.addSeparator();
		editMenu.add(find);
		editMenu.add(replace);
		editMenu.addSeparator();
		editMenu.add(selectAll);

		textarea.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent ke) {
				isModified = true;
			}

			public void keyReleased(KeyEvent ke) {
				if(ke.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
					checkForModification();
				}
			}
		});

		add(textarea);

		setSize(500, 500);
		setVisible(true);

		registerListeners();

		new AutoSaveThread(this);
	}

	public void checkForModification() {
		if(previousSavedData.equals(textarea.getText()))
			isModified = false;
		else if(! previousSavedData.equals(textarea.getText()))
			isModified = true;
	}

	private void registerListeners() {
		ActionEventHandler handler = new ActionEventHandler(this);
		exit.addActionListener(handler);
		open.addActionListener(handler);
		save.addActionListener(handler);
		saveAs.addActionListener(handler);
		find.addActionListener(handler);
		replace.addActionListener(handler);
		undo.addActionListener(handler);
		redo.addActionListener(handler);

		addWindowListener(new WindowEventHandler(this));
	}

	public void find(String search) {
		Pattern pattern = Pattern.compile(search);
		matcher = pattern.matcher(textarea.getText());
		if(matcher.find()) {
			textarea.select(matcher.start(), matcher.end());
			textarea.requestFocus();
		} else {
			System.out.println("No match found!");
		}
	}

	public void findNext() {
		if(matcher.find()) {
			textarea.select(matcher.start(), matcher.end());
			textarea.requestFocus();
		} else {
			matcher = matcher.reset();
			if(matcher.find()) {
				textarea.select(matcher.start(), matcher.end());
				textarea.requestFocus();
			}
		}
	}

	public void replaceAll(String search, String replaceText) {
		ArrayList<Integer> startIndex = new ArrayList<>();
		ArrayList<Integer> endIndex = new ArrayList<>();
		Pattern pattern = Pattern.compile(search);
		Matcher replaceMatcher = pattern.matcher(textarea.getText());
		while(replaceMatcher.find()) {
			startIndex.add(replaceMatcher.start());
			endIndex.add(replaceMatcher.end());
		}

		for(int i = startIndex.size() - 1; i >= 0; i--) {
			textarea.replaceRange(replaceText, startIndex.get(i), endIndex.get(i));
		}
	}

	public void undo() {
		if(! previousStates.isEmpty()) {
			futureStates.push(previousStates.pop());
			redo.setEnabled(true);
		}

		if(!previousStates.isEmpty()) {
			textarea.setText(previousStates.peek());
			textarea.setCaretPosition(textarea.getText().length());
		}
		else
			textarea.setText("");

		checkForModification();
	}

	public void redo() {
		if(! futureStates.isEmpty()) {
			textarea.setText(futureStates.peek());
			textarea.setCaretPosition(textarea.getText().length());
			futureStates.pop();
		} else {
			redo.setEnabled(false);
		}

		checkForModification();
	}
}