package CustomComponents.Notepad;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FindDialog extends Dialog {
	MainFrame parent;
	TextField findText;
	Button findButton;
	Button findNextButton;

	FindDialog(MainFrame parent) {
		super(parent, "Find Dialog", false);
		this.parent = parent;
		findText = new TextField();
		findButton = new Button("Find");
		findNextButton = new Button("Find Next");

		setLayout(new GridLayout(2, 1));
		Panel text = new Panel();
		text.setLayout(new BorderLayout());
		text.add(new Label("Find: ", Label.RIGHT), BorderLayout.WEST);
		text.add(findText, BorderLayout.CENTER);

		Panel buttons = new Panel();
		buttons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttons.add(findButton);
		buttons.add(findNextButton);

		add(text);
		add(buttons);

		registerListeners();

		setSize(300, 150);
		setVisible(true);
	}

	private void registerListeners() {
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ae) {
				dispose();
			}
		});

		ActionEventHandler handler = new ActionEventHandler(parent, this);
		findButton.addActionListener(handler);
		findNextButton.addActionListener(handler);
	}
}