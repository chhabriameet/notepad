package CustomComponents.Notepad;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.FileDialog;
import java.awt.Frame;

public class ActionEventHandler implements ActionListener {
	MainFrame frame;
	FindDialog findDialog;
	FindReplaceDialog replaceDialog;

	public ActionEventHandler(MainFrame frame) {
		this.frame = frame;
	}

	public ActionEventHandler(MainFrame frame, FindDialog findDialog) {
		this.frame = frame;
		this.findDialog = findDialog;
	}

	public ActionEventHandler(MainFrame frame, FindReplaceDialog replaceDialog) {
		this.frame = frame;
		this.replaceDialog = replaceDialog;
	}

	public void actionPerformed(ActionEvent ae) {
		if(findDialog == null && replaceDialog == null) {
			if(ae.getSource() == frame.exit) {
				System.exit(1);
			} else if(ae.getSource() == frame.save) {
				frame.fileOp.fileWrite(frame.textarea.getText());
				frame.isModified = false;
				frame.previousSavedData = frame.textarea.getText();
			} else if(ae.getSource() == frame.saveAs) {
				FileDialog fd = new FileDialog(frame, "Save File", FileDialog.SAVE);
				fd.setVisible(true);
				if(fd.getDirectory() != null && fd.getFile() != null) {
					String fileName = fd.getDirectory() + fd.getFile();
					if(! fileName.matches(".*\\.ht$"))
						fileName += ".ht";
					frame.fileOp = new FileOperations(fileName);
					frame.fileOp.fileWrite(frame.textarea.getText());
					frame.isModified = false;
					frame.previousSavedData = frame.textarea.getText();
					frame.saveAs.setEnabled(false);
					frame.save.setEnabled(true);
				}
			} else if(ae.getSource() == frame.undo) {
				frame.undo();
			} else if(ae.getSource() == frame.redo) {
				frame.redo();
			} else if(ae.getSource() == frame.find) {
				new FindDialog(frame);
			} else if(ae.getSource() == frame.replace) {
				new FindReplaceDialog(frame);
			} else if(ae.getSource() == frame.open) {
				FileDialog fd = new FileDialog(frame, "Open File", FileDialog.LOAD);
				fd.setVisible(true);
				if(fd.getDirectory() != null && fd.getFile() != null) {
					String fileName = fd.getDirectory() + fd.getFile();
					frame.fileOp = new FileOperations(fileName);
					String data = frame.fileOp.fileRead();
					frame.textarea.setText(data);
					frame.saveAs.setEnabled(false);
					frame.save.setEnabled(true);
				}
			}
		} else if(findDialog != null) {
			if(ae.getSource() == findDialog.findButton) {
				frame.find(findDialog.findText.getText());
			} else if(ae.getSource() == findDialog.findNextButton) {
				frame.findNext();
			}
		} else if(replaceDialog != null) {
 			if(ae.getSource() == replaceDialog.replaceButton) {
				frame.replaceAll(replaceDialog.findText.getText(), replaceDialog.replaceText.getText());
			}
		}
	}
}