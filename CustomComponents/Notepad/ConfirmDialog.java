package CustomComponents.Notepad;

import java.awt.Dialog;
import java.awt.Panel;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Button;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

public class ConfirmDialog extends Dialog implements DialogConstants, ActionListener {
	MainFrame parent;
	Button yes, no;
	Button cancel;
	static int result;
	ConfirmDialog(MainFrame parent, String messageText) {
		super(parent, "Modified", true);

		setLayout(new GridLayout(2, 1));

		this.parent = parent;

		Panel msgPanel = new Panel();
		Label message = new Label(messageText);
		msgPanel.setLayout(new GridLayout(1, 1));
		msgPanel.add(message);

		Panel buttons = new Panel();
		buttons.setLayout(new FlowLayout(FlowLayout.RIGHT));
		yes = new Button("Yes");
		no = new Button("No");
		buttons.add(yes);
		buttons.add(no);
		yes.addActionListener(this);
		no.addActionListener(this);
		cancel = new Button("Cancel");
		buttons.add(cancel);
		cancel.addActionListener(this);

		add(msgPanel);
		add(buttons);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				result = CANCEL;
				dispose();
			}
		});

		setSize(300, 120);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == yes) {
			result = YES;
			dispose();
		} else if(ae.getSource() == no) {
			result = NO;
			dispose();
		} else if(ae.getSource() == cancel) {
			result = CANCEL;
			dispose();
		}
	}

	private static int getResult() {
		return result;
	}

	public static int createDialog(MainFrame parent, String message) {
		new ConfirmDialog(parent, message);
		return getResult();
	}
}