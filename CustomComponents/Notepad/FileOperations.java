package CustomComponents.Notepad;

import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class FileOperations {
	File file;
	FileInputStream fis;
	BufferedReader br;
	FileOutputStream fout;

	public FileOperations(String fileName) {
		file = new File(fileName);
	}

	public String fileRead() {
		prepareInputStream();
		StringBuffer data = new StringBuffer();
		String line;
		try {
			while((line = br.readLine()) != null) {
				data.append(line + "\n");
			}
			closeInputStream();
			return data.toString();
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	public void fileWrite(String data) {
		prepareOutputStream(false);
		try {
			fout.write(data.getBytes());
		} catch (Exception e) {
			System.out.println(e);
		}
		closeOutputStream();
	}

	public void fileWrite(String data, boolean append) {
		prepareOutputStream(append);
		try {
			fout.write(data.getBytes());
		} catch (Exception e) {
			System.out.println(e);
		}
		closeOutputStream();
	}

	private void prepareInputStream() {
		try {
			fis = new FileInputStream(file);
			br = new BufferedReader(new InputStreamReader(fis));
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void prepareOutputStream(boolean append) {
		try {
			fout = new FileOutputStream(file, append);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void closeInputStream() {
		try {
			br.close();
			fis.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private void closeOutputStream() {
		try {
			fout.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}